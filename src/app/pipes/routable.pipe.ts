import { Pipe, PipeTransform } from '@angular/core';
import {Routes} from '@angular/router';

@Pipe({
  name: 'routable'
})
export class RoutablePipe implements PipeTransform {

  transform(value: Routes): Routes {
    let routes: Routes = [];

    value.forEach(r => {
      if(r.path != '')routes.push(r);
    });

    return routes;
  }

}
