import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Exo1Component} from './components/exo1/exo1.component';

export const routes: Routes = [
  { path: 'exo1/:id', component: Exo1Component },
  { path: '', component: Exo1Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
