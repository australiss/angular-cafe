import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { Exo1Component } from './components/exo1/exo1.component';
import { MenuComponent } from './components/menu/menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RoutablePipe } from './pipes/routable.pipe';
import { MarketModule } from './modules/market/market.module';

@NgModule({
  declarations: [
    AppComponent,
    Exo1Component,
    MenuComponent,
    RoutablePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MarketModule
  ],
  providers: [],
  bootstrap: [AppComponent, MenuComponent]
})
export class AppModule { }
