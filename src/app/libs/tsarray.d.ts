interface Array<T> {
  where(predicate: (item: T) => boolean);
}
