///<reference path="tsarray.d.ts"/>

if(!Array.prototype.where){
  Array.prototype.where = function* (predicate){

    for(let i = 0; i < this.length; i++){
      if(predicate(this[i])){
        yield this[i];
      }
    }

  };
}

