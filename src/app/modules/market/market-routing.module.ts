import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MarketComponent} from './components/market/market.component';
import {ProductListComponent} from './components/product-list/product-list.component';
import {CategorieFormCreateComponent} from './components/categorie-form-create/categorie-form-create.component';
import {LoginComponent} from './components/login/login.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'market', component: MarketComponent, children: [
    { path: 'products', component: CategorieFormCreateComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketRoutingModule { }
