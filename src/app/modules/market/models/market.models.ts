import {IButtonList} from './button-list.models';

export enum Role{
  ADMIN= 1,
  USER= 2
}

export interface ICategorie {
  id?: number;
  nom: string;
}
export interface IProduit {
  id?: number;
  reference?: string;
  nom: string;
  categorie: Categorie;
  prix: number;
  quantite?: number;
  url?: string;
}

export class Categorie implements ICategorie, IButtonList{
  id: number;
  nom: string;
  label: string;

  constructor(options?: ICategorie){
    this.id = options && options.id || 0;
    this.nom = options && options.nom || '';
    this.label = this.nom;
  }
}

export class Produit implements IProduit {
  categorie: Categorie;
  id: number;
  nom: string;
  prix: number;
  quantite: number;
  reference: string;
  url: string;

  constructor(options?: IProduit){
    this.id = options && options.id || 0;
    this.categorie = options && options.categorie || null;
    this.nom = options && options.nom || '';
    this.prix = options && options.prix || 0;
    this.quantite = options && options.quantite || 0;
    this.reference = options && options.reference || '';
    this.url = options && options.url || '';
  }
}


