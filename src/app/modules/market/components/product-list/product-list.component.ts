import {Component, Input, OnInit} from '@angular/core';
import {IProduit, Produit} from '../../models/market.models';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  @Input() produits: Array<Produit> = [];
  @Input() headers: Array<string> = [];

  constructor() { }

  ngOnInit() {
  }

  buyAction(produit: Produit, qttBuy: number) {
    produit.quantite -= qttBuy;
  }

  refillAction(produit: Produit, $event: number) {
    produit.quantite += $event;
  }
}
