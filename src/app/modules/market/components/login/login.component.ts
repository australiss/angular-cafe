import { Component, OnInit } from '@angular/core';
import {User} from '../../models/security.models';
import {Router} from '@angular/router';
import {AuthorizeService} from '../../services/authorize.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, public authoriser: AuthorizeService) { }

  ngOnInit() {
  }

  loginAction(form: HTMLFormElement){
    let username = form['username'].value;
    let password = form['password'].value;

    if(username == 'Flavian' && password == 'Blop'){
      let user = new User(username, password);
      sessionStorage.setItem('security.user', JSON.stringify(user));

      this.authoriser.login(user);
      this.router.navigate(['market']);
      return;
    }
  }
}
