import { Component, OnInit } from '@angular/core';
import {Categorie, Produit, Role} from '../../models/market.models';

import 'src/app/libs/tsarray';
import {environment} from 'src/environments/environment';

@Component({
  selector: 'market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.css']
})
export class MarketComponent implements OnInit {
  private _produits: Array<Produit> = [];
  private _categories: Array<Categorie> = [];
  produits: Array<Produit> = [];
  currentCat: number = 0;

  constructor() { }

  categories: Array<Categorie> = [];

  ngOnInit() {
    let dataProduct = localStorage.getItem(environment.storage_product_key);
    let dataCategorie = localStorage.getItem(environment.storage_categorie_key);

    if(dataProduct != null){
      this._produits = <Array<Produit>>JSON.parse(dataProduct);
    }
    if(dataCategorie!= null){
      this._categories = <Array<Categorie>>JSON.parse(dataCategorie);
      this.categories = this._categories;
    }

    this.afficher(0);
  }

  afficher(catToDisplay: number) {
    this.currentCat = catToDisplay;
    if(catToDisplay == 0){
      this.produits = this._produits;
      return;
    }
    this.produits = [];

    this.produits = this._produits.where(p => p.categorie.id == catToDisplay);
  }

  addNewProduct($event: Produit) {
    $event.id = this._produits.length == 0 ? 1 : this._produits[this._produits.length - 1].id + 1;
    this._produits.push($event);

    this.afficher(this.currentCat);
    localStorage.setItem(environment.storage_product_key, JSON.stringify(this._produits));
  }

  addNewCat($event: Categorie) {

    $event.id = this._categories.length == 0 ? 1 : this._categories[this._categories.length - 1].id + 1;

    this._categories.push($event);
    this.categories = this._categories;
    localStorage.setItem(environment.storage_categorie_key, JSON.stringify(this._categories));
  }
}
