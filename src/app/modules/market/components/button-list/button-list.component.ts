import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IButtonList} from '../../models/button-list.models';

@Component({
  selector: '[button-list]',
  templateUrl: './button-list.component.html',
  styleUrls: ['./button-list.component.css']
})
export class ButtonListComponent implements OnInit {
  @Input() items: Array<IButtonList>;
  @Output() selected: EventEmitter<number> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  clickAction(number: number) {
    this.selected.emit(number);
  }
}
