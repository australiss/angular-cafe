import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Categorie} from '../../models/market.models';

@Component({
  selector: 'categorie-form-create',
  templateUrl: './categorie-form-create.component.html',
  styleUrls: ['./categorie-form-create.component.css']
})
export class CategorieFormCreateComponent implements OnInit {
  @Output() created: EventEmitter<Categorie> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  submitAction(form: HTMLFormElement) {
    let nom = form['nom'].value;

    let cat = new Categorie({nom: nom});

    this.created.emit(cat);
  }
}
