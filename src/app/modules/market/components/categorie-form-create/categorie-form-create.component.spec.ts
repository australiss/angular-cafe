import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorieFormCreateComponent } from './categorie-form-create.component';

describe('CategorieFormCreateComponent', () => {
  let component: CategorieFormCreateComponent;
  let fixture: ComponentFixture<CategorieFormCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategorieFormCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorieFormCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
