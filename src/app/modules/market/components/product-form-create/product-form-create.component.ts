import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Categorie, Produit} from '../../models/market.models';

@Component({
  selector: 'product-form-create',
  templateUrl: './product-form-create.component.html',
  styleUrls: ['./product-form-create.component.css']
})
export class ProductFormCreateComponent implements OnInit {
  @Input() categories: Array<Categorie> = [];
  @Input() currentCat: number;
  @Output() created: EventEmitter<Produit> = new EventEmitter();
  constructor() { }

  ngOnInit() {
    console.log(this.categories);
  }

  submitAction(form: HTMLFormElement) {

    let nom = form['nom'].value;
    let prix = parseFloat(form['prix'].value);
    let categorie = parseInt(form['categorie'].value);

    let cat = new Categorie(this.categories.find(c => c.id == categorie));
    let newProd = new Produit({nom: nom, prix: prix, categorie: cat});

    this.created.emit(newProd);
  }
}
