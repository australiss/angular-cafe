import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IProduit} from '../../models/market.models';
import {Role, User} from '../../models/security.models';
import {AuthorizeService} from '../../services/authorize.service';

@Component({
  selector: '[product-list-item]',
  templateUrl: './product-list-item.component.html',
  styleUrls: ['./product-list-item.component.css']
})
export class ProductListItemComponent implements OnInit {
  @Input() produit: IProduit;
  @Input() fieldToProject: Array<string> = [];
  @Output() buy: EventEmitter<number> = new EventEmitter<number>();
  @Output() refused: EventEmitter<void> = new EventEmitter<void>();
  @Output() refill: EventEmitter<number> = new EventEmitter<number>();


  constructor(public authorize: AuthorizeService) {
  }

  ngOnInit() {


  }
  buyAction(qttBuy: HTMLInputElement) {
    console.log(`${qttBuy.max} => ${qttBuy.value}`);
    if(qttBuy.min >= qttBuy.value && qttBuy.max <= qttBuy.value)return;
    if(this.produit.quantite < parseInt(qttBuy.value)){
      this.refused.emit();
      return;
    }

    this.buy.emit(parseInt(qttBuy.value));
  }

  isProject(id: string): boolean {
    return this.fieldToProject.find(item => item == id) != null;
  }

  refillAction(qtt: HTMLInputElement) {
    this.refill.emit(parseInt(qtt.value));
  }
}
