import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductListItemComponent } from './components/product-list-item/product-list-item.component';
import {MarketComponent} from './components/market/market.component';
import { ButtonListComponent } from './components/button-list/button-list.component';
import { ProductFormCreateComponent } from './components/product-form-create/product-form-create.component';
import {FormsModule} from '@angular/forms';
import { CategorieFormCreateComponent } from './components/categorie-form-create/categorie-form-create.component';
import {MarketRoutingModule} from './market-routing.module';
import { LoginComponent } from './components/login/login.component';
import {AuthorizeService} from './services/authorize.service';

@NgModule({
  declarations: [
    MarketComponent,
    ProductListComponent,
    ProductListItemComponent,
    ButtonListComponent,
    ProductFormCreateComponent,
    CategorieFormCreateComponent,
    LoginComponent],
  exports: [ MarketRoutingModule ],
  imports: [
    CommonModule,
    FormsModule,
    MarketRoutingModule
  ],
  providers: [AuthorizeService]
})
export class MarketModule { }
