import { Component, OnInit } from '@angular/core';
import {Categorie} from '../../modules/market/models/market.models';

@Component({
  selector: 'exo1',
  templateUrl: './exo1.component.html',
  styleUrls: ['./exo1.component.css'],
})
export class Exo1Component implements OnInit {
  title: string = 'Ma première application angular!';
  bgColor: string;
  txtColor: string;

  constructor() { }

  ngOnInit() {

  }

  changeTheType($event: Event, theType: HTMLSelectElement) {
    console.log(`event => ${event}, theType => ${theType.value}`);
  }
}
