import {Component, OnInit} from '@angular/core';
import {Routes} from '@angular/router';
import {routes} from '../../app-routing.module';
import {environment} from '../../../environments/environment';
import {AuthorizeService} from '../../modules/market/services/authorize.service';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  routes: Routes;

  constructor(public authorize: AuthorizeService) { }

  ngOnInit() {
    this.routes = routes;

   }

}
